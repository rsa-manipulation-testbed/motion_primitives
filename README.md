# Motion Primitives

This package has implementations of basic motions for the Bravo arm, including grasp execution.

The core capability is in `motion.py`. A demo script is given in `execute_grasps.py`.
```
launch
| - grasp_exec_demo.launch  # launch file to run grasp exec demo

src
| - grasp_exec_demo.py      # demo script to execute grasp
| - motion.py               # module with motion primitives
```

# motion.py capabilities

This module contains a class, Mover, meant to interface with MoveIt through its Python API. A MoveIt Move Group has to be running on the same roscore. `motion.py` was developed based on the [MoveIt Python Interface tutorials](https://ros-planning.github.io/moveit_tutorials/doc/move_group_python_interface/move_group_python_interface_tutorial.html). 

Refer to the in-code documentation for Mover's API. The functions are:

- `__init__`: does some of the confusing MoveGroup initialization stuff
- `add_ground_plane_to_planning_scene`
- `go_joints`
- `go_gripper`
- `go_ee_pose`
- `go_named_group_state`
- `execute_grasp`: uses the above function to execute a grasp

# Details and Limitations of `grasp_exec_demo.py`
The demo script uses the Mover object's `execute_grasp()` function to move the end effector first to a pre-grasp pose and then to the final grasp pose that is specified as a command line argument to `grasp_exec_demo.py`. Specifically, the executed sequence is:

1) Open the jaws
2) Move the end effector to the orbital pose, which is a pre-grasp pose offset from final pose by a distance `peck_distance` along the gripper axis.
3) Move the end effector to the final pose
4) Close the jaws
5) Move the end effector to the orbital pose
6) Move the arm to the 'rest' configuration

Each of these behaviors is implemented using functions from the Mover class. However, this simple sequence belies a number of limitations:

 * the specified final grasp pose and its corresponding orbital pose must be kinematically feasible, collision free, and reachable.
 * each step of the pipeline is planned and executed in sequence rather than validating an end-to-end plan prior to execution, which may cause failures in the middle steps. See, e.g., [MoveIt Task Constructor](https://ros-planning.github.io/moveit_tutorials/doc/moveit_task_constructor/moveit_task_constructor_tutorial.html) for an example of a strategy that plans an entire sequence prior to beginning motion.
 * collisions between the gripper and the scene are not robustly avoided. We add a single collision object to MoveIt's Planning Scene to avoid collisions between the arm and the table; however, MoveIt can still plan motions that collide with obstacles that are not explicitly added to the Planning Scene. For example, a motion might squeeze the manipuland strongly into the table. Or, after securing the manipuland in the grippers, the robot arm may sweep the manipuland into other elements of the scene because it does not model those collision elements.

Additionally, the robot's software stack involves a complex set of many interacting processes, and may have undiscovered bugs. The launch file at [bravo_arm_sw/launch/bravo_arm.launch](https://gitlab.com/rsa-manipulation-testbed/bravo_arm_sw/-/blob/main/launch/bravo_arm.launch) describes and invokes these processes.

# Installation

First follow the [central installation instructions](https://gitlab.com/rsa-manipulation-testbed/bravo_arm_sw/-/blob/main/README.md) in the `bravo_arm_sw` to install the Bravo dependencies for the real arm and simulation.

Then, clone this repo and build it into your catkin workspace:
```
cd src
git clone https://<this_repo_url>.git
catkin build
cd ..
source devel/setup.bash
```

> Note on Supported Versions:
> ---
> This repo was developed and tested with the following repo versions:
>
> ```
> === ./src/bpl_bravo_description_real (git) ===
> 5df844a
> === ./src/bpl_bravo_moveit_config (git) ===
> bce48c2
> === ./src/bravo_arm_sw (git) ===
> adf2129
> === ./src/libbpl_protocol (git) ===
> 6b087c2
> === ./src/rsa_bravo_driver (git) ===
> 08c73e3
> === ./src/rsa_bravo_gazebo (git) ===
> 44c8613
> === ./src/rsa_bravo_msgs (git) ===
> e517065
> ```

# Usage
## Simulated Bravo

After building this package and its dependencies and sourcing `setup.bash`, run the simulated Bravo with:
```
roslaunch motion_primitives grasp_exec_demo.launch simulated:=true
```

A number of xterm windows will pop up, one of which is for `grasp_exec_demo.py`. When prompted to press enter, read the prompt completely before complying. A video of the result is [here](https://www.youtube.com/watch?v=FpuuFgcmZSU).

![grasp_exec_sim](figs/grasp_exec_sim.png)

## Real Bravo

After building this package and its dependencies and sourcing `setup.bash`, run the real Bravo with:
```
roslaunch motion_primitives grasp_exec_demo.launch simulated:=false
```

A number of xterm windows will pop up, one of which is for `grasp_exec_demo.py`. Following the prompt in the Python window, **be sure to enable the Bravo driver (with the workspace clear of personnel) BEFORE hitting enter.** Otherwise, the JointTrajectoryController may set the joint positions to a configuration that is far from the initial configuration, resulting in an alarmingly fast motion when the arm _is_ enabled. To enable the arm, issue `rosservice call /bravo/enable_bravo 1` in another terminal.

 A video of the result is [here](https://www.youtube.com/watch?v=2-gofXsM5zM).

![grasp_exec_real](figs/grasp_exec_real.png)


# Debugging and Development

The functions in `motion.py` are intended to be branched, forked, and modified. Run with them! They should be a reasonably stable baseline.

To debug problems related to this motion stack, I recommend the following workflow:

1) Understand the purpose of all of the processes launched in `bravo_arm_ws/launch/bravo_arm.launch`. This launch file loads the robot's URDF, states the ROSControl controller, starts the Move Group, and starts `moveit_servo` and `teleop_joy_twist` for Cartesian velocity control. Combined with the [`rqt_graph`](http://wiki.ros.org/rqt_graph) utility, this launch file is a good start to understand which ROS nodes are interacting.

1) Place print statements inside the code of interest -- _even if it's not your code_. All of these repos, as well as MoveIt, OMPL, and ROSControl, can be cloned into your workspace and built from source, making it easy to add print statements.

1) Use Visual Studio Code's visual debugger. It's [well-documented](https://github.com/ms-iot/vscode-ros/blob/master/doc/debug-support.md) and lets you set breakpoints to step through code as it's running. Remember to compile your workspace with `catkin build -DCMAKE_BUILD_TYPE=Debug` to build debug symbols into executables.

1) Seek help on StackOverflow, the MoveIt Discord, ROSAnswers, or Github. The developers in this ecosystem are friendly and appreciative of Issues that users raise in Github's ticketing system and encourage users to submit Pull Requests to fix them.

# Additional References
* A great [ROSControl reference](https://nu-msr.github.io/me495_site/ros_control.html)
* The [MoveIt Noetic docs](https://ros-planning.github.io/moveit_tutorials/)