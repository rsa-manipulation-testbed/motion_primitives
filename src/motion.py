#! /usr/bin/python3

import moveit_commander
import sys

from geometry_msgs.msg import PoseStamped
import numpy as np

import rospy
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import time

class Mover:
    """Wrapper around MoveIt functionality for the arm."""

    arm_move_group_cmdr: moveit_commander.MoveGroupCommander
    arm_robot_cmdr: moveit_commander.RobotCommander
    arm_group_name: str
    scene: moveit_commander.PlanningSceneInterface
    gripper_pub: rospy.Publisher
    box_name: str = None
    grasping_group_name: str

    def __init__(self):
        """Initialize moveit_commander to control the arm. 
        Start a publisher to talk to the hand_position_controller.
        Add a large ground plane to the Planning Scene."""

        moveit_commander.roscpp_initialize(sys.argv)
        self.arm_group_name = "arm"
        self.grasping_group_name = "hand"
        self.arm_robot_cmdr = moveit_commander.RobotCommander(
            robot_description="robot_description"
        )
        self.arm_move_group_cmdr = moveit_commander.MoveGroupCommander(
            self.arm_group_name, robot_description="robot_description"
        )

        self.scene = moveit_commander.PlanningSceneInterface(synchronous=True)
        self.gripper_pub = rospy.Publisher('hand_position_controller/command', 
            data_class=JointTrajectory, queue_size=1
        )
        self.add_ground_plane_to_planning_scene()

        self.arm_move_group_cmdr.set_planner_id("RRTConnect")

    def add_ground_plane_to_planning_scene(self):
        """Add a box object to the PlanningScene to prevent paths that collide
        with the ground. """

        box_pose = PoseStamped()
        box_pose.header.frame_id = self.arm_robot_cmdr.get_planning_frame()
        box_pose.pose.orientation.w = 1.0
        box_pose.pose.position.z = -0.5
        box_name = "ground_plane"
        self.scene.add_box(box_name, box_pose, size=(3, 3, 1))

    def go_joints(self, joints: np.ndarray, wait: bool = True):
        """Move robot to the given joint configuration.

        Args:
            joints (np.ndarray): joint angles
            wait (bool): whether to block until finished 
        """
        success = self.arm_move_group_cmdr.go(joints, wait=wait)
        self.arm_move_group_cmdr.stop()
        return success

    def go_gripper(self, pos: np.ndarray, wait: bool = True) -> bool:
        """Move the gripper fingers to the given actuator value.

        Args:
            pos (np.ndarray): desired position of `bravo_axis_a`
            wait (bool): whether to wait until it's probably done
        """
        jt = JointTrajectory()
        jt.joint_names = ['bravo_axis_a']
        jt.header.stamp = rospy.Time.now()

        jtp = JointTrajectoryPoint()
        jtp.positions = pos
        jtp.time_from_start = rospy.Duration(secs=3)
        jt.points.append(jtp)

        self.gripper_pub.publish(jt)

        if wait:
            time.sleep(3)

        return True

    def go_ee_pose(self, pose: PoseStamped, wait: bool = True) -> bool:
        """Move the end effector to the given pose.

        Args:
            pose (geometry_msgs.msg.Pose): desired pose for end effector
            wait (bool): whether to block until finished
        """

        pose.header.stamp = rospy.Time.now() # the timestamp may be out of date.
        self.arm_move_group_cmdr.set_pose_target(pose, end_effector_link="ee_link")

        success = self.arm_move_group_cmdr.go(wait=wait)
        self.arm_move_group_cmdr.stop()
        self.arm_move_group_cmdr.clear_pose_targets()

        return success
    
    def go_named_group_state(self, state: str, wait: bool = True) -> bool:
        """Move the arm group to a named state from the SRDF.

        Args:
            state (str): the name of the state
            wait (bool, optional): whether to block until finished. Defaults to True.
        """
        self.arm_move_group_cmdr.set_named_target(state)
        success = self.arm_move_group_cmdr.go(wait=wait)
        self.arm_move_group_cmdr.stop()

        return success

    def execute_grasp(self, orbital_pose: PoseStamped, final_pose: PoseStamped) -> bool:
        """Execute a grasp by planning and executing a sequence of motions in turn:
           1) Open the jaws
           2) Move the end effector the the orbital pose
           3) Move the end effector to the final pose
           4) Close the jaws
           5) Move the end effector to the orbital pose
           6) Move the arm to the 'rest' configuration

        Args:
            orbital_pose (PoseStamped): the pre-grasp orbital pose of the end effector
            final_pose (PoseStamped): the end effector pose in which to close the jaws

        Returns:
            bool: whether every step succeeded according to MoveIt
        """

        print("Executing grasp.")

        print("\tOpening jaws.")
        success = self.go_gripper(np.array([0.02]), wait=True)
        if not success: return False

        print("\tMoving end effector to orbital pose.")
        success = self.go_ee_pose(orbital_pose, wait=True)
        if not success: return False

        print("\tMoving end effector to final pose.")
        success = self.go_ee_pose(final_pose, wait=True)
        if not success: return False

        print("\tClosing jaws.")
        success = self.go_gripper(np.array([0.0]), wait=True)
        if not success: return False

        print("\tMoving end effector to orbital pose.")
        success = self.go_ee_pose(orbital_pose, wait=True)
        if not success: return False

        print("\tMoving arm to 'rest' configuration.")
        success = self.go_named_group_state('rest', wait=True)
        return success