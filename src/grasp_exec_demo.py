#!/usr/bin/python3

import rospy
import argparse
from geometry_msgs.msg import PoseStamped, Vector3, Pose, Quaternion
import numpy as np
from tf.transformations import quaternion_from_euler, quaternion_matrix

from motion import Mover

rospy.init_node('grasp_exec_demo')

rospy.loginfo("Waiting for MoveIt services.")
rospy.wait_for_service("/bravo/plan_kinematic_path") # wait for moveit to be up and running
rospy.wait_for_service('/bravo/apply_planning_scene')

def orbital_pose(goal_pose: PoseStamped, peck_distance: float) -> Pose:
    """ Return a pre-grasp orbital pose that is offset from the final goal pose
    along the z direction.

    Args:
        goal_pose (PoseStamped): the final goal pose of the end effector (defined 
            as 'ee_link' in bpl_bravo_moveit_config)
        peck_distance (float): the distance from the orbital pose to the final goal pose

    Returns:
        orbit_pose (Pose): the pre-grasp orbital pose
    """
    o = goal_pose.pose.orientation
    rot = quaternion_matrix(np.array([o.x, o.y, o.z, o.w]))
    z_hat = rot[:3, 2]
    p = goal_pose.pose.position
    pos = np.array([p.x, p.y, p.z])
    pos -= z_hat * peck_distance
    pos = Vector3(x=pos[0],y=pos[1],z=pos[2])
    orbit_pose = PoseStamped(
        header=goal_pose.header,
        pose=Pose(position=pos, orientation=o)
    )
    return orbit_pose

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-x", type=float,
        help="x location of pose of 'ee_link' at which to grasp.", default=0.37)
    parser.add_argument("-y", type=float,
        help="y location of pose of 'ee_link' at which to grasp.", default=-0.2)
    parser.add_argument("-z", type=float,
        help="z location of pose of 'ee_link' at which to grasp.", default=0.2)
    parser.add_argument("-roll", type=float,
        help="roll of pose of 'ee_link' at which to grasp.", default=3.14)
    parser.add_argument("-pitch", type=float,
        help="pitch of pose of 'ee_link' at which to grasp.", default=-0.28)
    parser.add_argument("-yaw", type=float,
        help="yaw of pose of 'ee_link' at which to grasp.", default=-1.15)
    parser.add_argument("-peck_distance", type=float,
        help="Distance that the orbital pose is retracted from the final goal pose.", default=0.1)
    parser.add_argument("-frame_id", type=str,
        help="frame_id in which the grasp pose is expressed.", default="world")           

    args, unknown = parser.parse_known_args()

    # Construct the final goal pose and orbital pose from the script args
    p = Pose()
    p.position = Vector3(args.x, args.y, args.z)
    p.orientation = Quaternion(*quaternion_from_euler(args.roll, args.pitch, args.yaw))

    final_grasp_pose = PoseStamped()
    final_grasp_pose.header.stamp = rospy.Time.now()
    final_grasp_pose.header.frame_id = args.frame_id
    final_grasp_pose.pose = p

    orbit_pose = orbital_pose(final_grasp_pose, peck_distance=args.peck_distance)

    mover = Mover() # Instantiate an object to control the robot arm

    # Prompt the user to press enter, then execute the grasp.
    print(f"The final grasp pose is \n{final_grasp_pose}.")
    input("If using the real Bravo, ENABLE THE BRAVO BEFORE EXECUTION. \
        \nPress enter to execute the grasp. Press CTRL-C to exit.")

    print("Moving arm to 'rest' configuration.")
    mover.go_named_group_state('rest', wait=True)

    mover.execute_grasp(orbit_pose, final_grasp_pose)

if __name__ == "__main__":
    main()